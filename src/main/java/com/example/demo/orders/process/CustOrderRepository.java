package com.example.demo.orders.process;

import org.springframework.data.repository.CrudRepository;

public interface CustOrderRepository extends CrudRepository<CustOrder, Long>{
    
}
