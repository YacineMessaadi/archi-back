package com.example.demo.orders.process;

import com.example.demo.customers.process.Customer;
import com.example.demo.products.process.Product;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
public class CustOrder  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "order_id")
	private Long id;
	private String name;
	private int status;
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnoreProperties({"orders"})
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	@ManyToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinTable(name = "order_product",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "product_id"))
    private Set<Product> products = new HashSet<>();

	public CustOrder() {
		super();
	}
	
	public CustOrder(String name, Customer customer, Set<Product> products) {
		super();
		this.name = name;
		this.customer = customer;
		this.status = 0;
		this.products = products;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}