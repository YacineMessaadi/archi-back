package com.example.demo.orders.resource;

import com.example.demo.orders.process.CustOrder;
import com.example.demo.orders.process.CustOrderRepository;
import com.example.demo.products.process.Product;

import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Path("Orders")
public class CustOrderResource {
	@Autowired
	private CustOrderRepository custOrderRepository;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CustOrder createOrder(CustOrder o) {
		return custOrderRepository.save(o);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CustOrder> getAllOrders() {
		List<CustOrder> orders = new ArrayList<>();
		custOrderRepository.findAll().forEach(orders::add);
		return orders;
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CustOrder updateTotalyOrder(@PathParam("id") Long id, CustOrder o) {
		o.setId(id);
		return custOrderRepository.save(o);
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteOrder(@PathParam("id") Long id) {
		if (custOrderRepository.findById(id).isPresent()) {
			custOrderRepository.deleteById(id);
		}
		return Response.noContent().build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrderById(@PathParam("id") Long id) {
		Optional<CustOrder> p = custOrderRepository.findById(id);
		if (p.isPresent()) {
			return Response.ok(p.get()).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	// PATCH /Orders/{id}
	public Response updateStatus(@PathParam("id") Long id, int status) {
		Optional<CustOrder> optional = custOrderRepository.findById(id);

		if (optional.isPresent()) {
			CustOrder oBDD = optional.get();
			oBDD.setStatus(status);
			custOrderRepository.save(oBDD);
			return Response.ok(oBDD).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@GET
	@Path("{id}/products")
	@Produces(MediaType.APPLICATION_JSON)
	// GET /Orders/{id}/products
	public Set<Product> listProducts(@PathParam("id") Long id) {
		return custOrderRepository.findById(id).get().getProducts();
	}

}