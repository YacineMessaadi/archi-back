package com.example.demo.customers.resource;

import com.example.demo.orders.process.CustOrder;
import com.example.demo.orders.process.CustOrderRepository;
import com.example.demo.customers.process.Customer;
import com.example.demo.customers.process.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Path("Customers")
public class CustomerResource {
	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustOrderRepository custOrderRepository;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Customer createCustomer(Customer c) {
		return customerRepository.save(c);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> getAllCustomer() {
		List<Customer> Customers = new ArrayList<>();
		customerRepository.findAll().forEach(Customers::add);
		return Customers;
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Customer updateTotalyCustomer(@PathParam("id") Long id, Customer c) {
		c.setId(id);
		return customerRepository.save(c);
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCustomer(@PathParam("id") Long id) {
		if (customerRepository.findById(id).isPresent()) {
			Customer c = customerRepository.findById(id).get();
			c.getOrders().forEach(custOrder -> custOrderRepository.deleteById(custOrder.getId()));
			customerRepository.deleteById(id);
		}
		return Response.noContent().build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerById(@PathParam("id") Long id) {
		Optional<Customer> c = customerRepository.findById(id);
		if (c.isPresent()) {
			return Response.ok(c.get()).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@GET
	@Path("{id}/orders")
	@Produces(MediaType.APPLICATION_JSON)
	// GET /Customers/{id}/orders
	public Set<CustOrder> getOrders(@PathParam("id") Long id) {
		return customerRepository.findById(id).get().getOrders();
	}

	// a POST request to add an order existing in custOrderRepository to a customer
	@POST
	@Path("{id}/orders")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	// POST /Customers/{id}/orders
	public Response addOrder(@PathParam("id") Long id, CustOrder o) {
		Optional<Customer> c = customerRepository.findById(id);
		if (c.isPresent()) {
			c.get().getOrders().add(o);
			o.setCustomer(c.get());
			return Response.ok(custOrderRepository.save(o)).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}