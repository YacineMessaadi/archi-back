package com.example.demo.customers.process;

import com.example.demo.orders.process.CustOrder;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
public class Customer  {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	private String name;
	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "customer", fetch = FetchType.EAGER)
	private Set<CustOrder> orders = new HashSet<CustOrder>();
	
	public Customer() {
		super();
	}
	
	public Customer(String name) {
		super();
		this.name = name;
		this.orders = new HashSet<CustOrder>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<CustOrder> getOrders() {
		return orders;
	}
	
	public void setOrders(Set<CustOrder> orders) {
		this.orders = orders;
	}
}