package com.example.demo.products.resource;

import com.example.demo.products.process.Product;
import com.example.demo.products.process.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("Products")
public class ProductResource {
	@Autowired
	private ProductRepository ProductRepository;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product createProduct(Product p) {
		return ProductRepository.save(p);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllProducts() {
		List<Product> products = new ArrayList<>();
		ProductRepository.findAll().forEach(products::add);
		return products;
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product updateTotalyProduct(@PathParam("id") Long id, Product p) {
		p.setId(id);
		return ProductRepository.save(p);
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProduct(@PathParam("id") Long id) {
		Optional<Product> p = ProductRepository.findById(id);
		if (p.isPresent()) {
			ProductRepository.deleteById(id);
		}
		return Response.noContent().build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductById(@PathParam("id") Long id) {
		Optional<Product> p = ProductRepository.findById(id);
		if (p.isPresent()) {
			return Response.ok(p.get()).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	// PATCH /Products/{id}
	public Response updatePrice(@PathParam("id") Long id, Product p) {
		Optional<Product> optional = ProductRepository.findById(id);

		if (optional.isPresent()) {
			Product pBDD = optional.get();
			ProductRepository.save(pBDD);
			return Response.ok(pBDD).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}