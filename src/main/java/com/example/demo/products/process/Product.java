package com.example.demo.products.process;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.example.demo.orders.process.CustOrder;


@Entity
public class Product  {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name = "product_id")
	private Long id;
	private String name;
	private String description;
	private String image;
	private double price;

	@ManyToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinTable(name = "order_product",
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "order_id", referencedColumnName = "order_id"))
    private Set<CustOrder> orders = new HashSet<>();
	
	public Product() {
		super();
	}
	
	public Product(String name, String description, double price, String image) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.image = image;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setOrders(Set<CustOrder> orders) {
		this.orders = orders;
	}

	public List<Long> getOrders() {
		List<Long> orderIds = new ArrayList<Long>();
		for (CustOrder order : orders) {
			orderIds.add(order.getId());
		}
        return orderIds;
    }
}