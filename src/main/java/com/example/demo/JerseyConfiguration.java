package com.example.demo;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.stereotype.Component;
import javax.ws.rs.ApplicationPath;

import com.example.demo.customers.resource.CustomerResource;
import com.example.demo.orders.resource.CustOrderResource;
import com.example.demo.products.resource.ProductResource;

@Component
@ApplicationPath("api")
public class JerseyConfiguration extends ResourceConfig {
	
	public JerseyConfiguration() {
		register(CORSResponseFilter.class);
		register(CustomerResource.class);
		register(CustOrderResource.class);
		register(ProductResource.class);

		
		property(ServletProperties.FILTER_FORWARD_ON_404, true);
	}
}
